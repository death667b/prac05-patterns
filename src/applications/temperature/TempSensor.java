package applications.temperature;

import java.util.Random;
import java.util.Observable;
//import patterns.observer.Subject;

/**
 * The Concrete Subject class for the Observer Pattern example 
 * @author Richard Thomas
 *
 */
public class TempSensor extends Observable {
	private double temp; 
	private double low; 
	private double high;
	private Random rng; 
 
	
	public TempSensor(long seed, double high, double low) {
		rng = new Random(seed); 
		this.high = high;
		this.low = low;
	}
	
	public void setState() {
		double range = high - low;
		temp = Math.round(rng.nextDouble() * range + low);
		setChanged();
	}
	
	public String getState() {
		return Double.toString(temp);
	}

	public void NONONONONONONO(){
		
	}
}
